## Windows 10 Github RDP ft Ngrok

![.](spec.png)

# Read This Before Rushing To Actions Tab 💀

* Note : i'm not responsible for suspended github accounts
* Note : Those who have low connection use no sound workflow

### Windows 10 Least

VM features:
- AMD EPYC 7763 64-Core Processor 2.44 GHz
- 16 GB RAM
- 100 GB Disk **(Excluded System Used)**

---

* We Have Some Cool Features That Other workflows Dosen't Have
  - Automatically Telegram Installed
  - Automatically Winrar Installed
  - Automatically VM Quick Config Installed
  - Removed Stupid/Unrated Softwares
  - Ect ...

## Deploy and Run

<details>
    <summary>Setup Secret Key</summary>
<br>

* Create new github repo
    
* Go to **Settings in repo > Secrets and variables > Actions**.
    
* Click **New repository secret**.
   
* To **Name: NGROK_AUTH_TOKEN** and to **Secret: paste your ngrok auth token [get your token](https://dashboard.ngrok.com/get-started/your-authtoken)**.

* Now click **Add Secret**.
</details>

<details>
    <summary>Windows 10 RDP Install and Run</summary>
<br>
    
* Just Download A Workflow from Releases.
    
* Click **create new file** and copy this text **.github/workflows/test** also type test in empty box and click **committed changes** after that **upload downloaded workflow in there**.
    
* Now go to **Actions** Tab and select workflow.

* Click **Run Workflow** button on the left of **This workflow has a workflow_dispatch event trigger** line.

* Wait few minutes.

* Copy the link(**without tcp://**) and go to Remote Desktop, paste the link to connect that you copied from the workflow run.

* Fill in those login info, within **username:TheDisala** or **runneradmin** and **password:TheDisa1a** .

* Enjoy!

</details>

# [Watch Tutorial If You Dosen't Understand This.](https://youtu.be/P-ctz1CuPi0)

### Brought To You By Disala 💀 , Its Functional 😗.
### You Can See IP , Username , Pass And Cool Ascki Art 
